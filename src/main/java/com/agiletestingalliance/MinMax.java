package com.agiletestingalliance;

public class MinMax {

    public int getMax(int left, int right) {
        if (right > left) {
            return right;
	} else {
            return left;
	}
    }
}
