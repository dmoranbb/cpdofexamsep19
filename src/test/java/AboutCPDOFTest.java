import com.agiletestingalliance.AboutCPDOF;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author <a href="mailto:david.moran@britebill.com">David Morán</a>
 */
public class AboutCPDOFTest {

    @Test
    public void testDesc() {
        String desc = new AboutCPDOF().getMessage();

        assertThat(desc).isNotBlank();
        assertThat(desc).startsWith("CP-DOF certification")
            .endsWith("solidify your learnings.");
    }
}
