import com.agiletestingalliance.Duration;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author <a href="mailto:david.moran@britebill.com">David Morán</a>
 */
public class DurationTest {

    @Test
    public void testDur() {
        String message = new Duration().getMessage();

        assertThat(message).isNotBlank();
        assertThat(message).startsWith("CP-DOF is designed specifically")
            .endsWith("practical exams.");
    }
}
