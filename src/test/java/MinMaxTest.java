import com.agiletestingalliance.MinMax;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author <a href="mailto:david.moran@britebill.com">David Morán</a>
 */
public class MinMaxTest {

    @Test
    public void testFWithFirstParameterGreaterThanSecond() {
        int result = new MinMax().getMax(9, 1);

        assertThat(result).isEqualTo(9);
    }

    @Test
    public void testFWithFirstParameterLesserThanSecond() {
        int result = new MinMax().getMax(1, 9);

        assertThat(result).isEqualTo(9);
    }

    @Test
    public void testFWithEqualParameters() {
        int result = new MinMax().getMax(9, 9);

        assertThat(result).isEqualTo(9);
    }
}
