import com.agiletestingalliance.Usefulness;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author <a href="mailto:david.moran@britebill.com">David Morán</a>
 */
public class UsefulnessTest {

    @Test
    public void testDesc() {
        String result = new Usefulness().getDescription();

        assertThat(result).isNotBlank();
        assertThat(result).startsWith("DevOps is about transformation")
            .endsWith("concepts and mindset.");
    }
}
